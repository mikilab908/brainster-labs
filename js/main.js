// giving an event listener on the checkboxes
let check1 = document.querySelector('#checkbox1');
if (check1) {
  check1.addEventListener('click', showMarketCards);
}

let check2 = document.querySelector('#checkbox2');
if (check2) {
  check2.addEventListener('click', showProgramCards);
}

let check3 = document.querySelector('#checkbox3');
if (check3) {
  check3.addEventListener('click', showDesignCards);
}

// seting up the marketing cards
// function for unchecking other checkboxes
function showMarketCards() {
  document.querySelector('#checkbox2').checked = false;
  document.querySelector('#checkbox3').checked = false;

  // first we gona hide all cards
  let allCards = document.querySelectorAll('.karta');
  allCards.forEach(function (card) {
    card.style.display = 'none';
  });

  // show the cards with the current class
  if (document.querySelector('#checkbox1').checked) {
    let marketCards = document.querySelectorAll('.onMarket');
    marketCards.forEach(function (marketCard) {
      marketCard.style.display = 'inline-block';
    });
  } else {
    let allCards = document.querySelectorAll('.karta');
    allCards.forEach(function (card) {
      card.style.display = 'inline-block';
    });
  }
}

// seting up the programing cards
// function for unchecking other checkboxes
function showProgramCards() {
  document.querySelector('#checkbox1').checked = false;
  document.querySelector('#checkbox3').checked = false;

  // first we gona hide all cards
  let allCards = document.querySelectorAll('.karta');
  allCards.forEach(function (card) {
    card.style.display = 'none';
  });

  // show the cards with the current class
  if (document.querySelector('#checkbox2').checked) {
    let programCards = document.querySelectorAll('.onProgram');
    programCards.forEach(function (programCard) {
      programCard.style.display = 'inline-block';
    });
  } else {
    let allCards = document.querySelectorAll('.karta');
    allCards.forEach(function (card) {
      card.style.display = 'inline-block';
    });
  }
}

// seting up the design cards
// function for unchecking other checkboxes
function showDesignCards() {
  document.querySelector('#checkbox1').checked = false;
  document.querySelector('#checkbox2').checked = false;

  // first we gona hide all cards
  let allCards = document.querySelectorAll('.karta');
  allCards.forEach(function (card) {
    card.style.display = 'none';
  });

  // show the cards with the current class
  if (document.querySelector('#checkbox3').checked) {
    let designCards = document.querySelectorAll('.onDesign');
    designCards.forEach(function (designCard) {
      designCard.style.display = 'inline-block';
    });
  } else {
    let allCards = document.querySelectorAll('.karta');
    allCards.forEach(function (card) {
      card.style.display = 'inline-block';
    });
  }
}

// function for the load-more button
let loadCards = document.querySelector('#load-more');
if (loadCards) {
  loadCards.addEventListener('click', loadMoreCards);
}

function loadMoreCards() {
  let allCards = document.querySelectorAll('.karta');
  let cardsShownAfterClick = 0;

  allCards.forEach(function (card) {
    let allCssProps = window.getComputedStyle(card);
    if (allCssProps.display == 'none') {
      if (cardsShownAfterClick < 6) {
        card.style.display = 'inline-block';
        cardsShownAfterClick = cardsShownAfterClick + 1;
      }
    }
  });

  // when all the cards are loaded turn-off the load button
  let numOfHiddenCards = 0;

  allCards.forEach(function (card) {
    let allCssProps = window.getComputedStyle(card);
    if (allCssProps.display == 'none') {
      numOfHiddenCards = numOfHiddenCards + 1;
    }
  });

  if (numOfHiddenCards == 0) {
    document.querySelector('#load-more').style.display = 'none';
  }
}

// selected option menu function
const selected = document.querySelector('.selected');
const optionsContainer = document.querySelector('.options-container');

const optionList = document.querySelectorAll('.option');

if (selected) {
  selected.addEventListener('click', () => {
    optionsContainer.classList.toggle('active');
  });
}

optionList.forEach((o) => {
  o.addEventListener('click', () => {
    selected.innerHTML = o.querySelector('label').innerHTML;
    optionsContainer.classList.remove('active');
  });
});

// validation function for select option menu
let subBtn = document.querySelector('#submitBtn');
if (subBtn) {
  subBtn.addEventListener('click', validate);
}

const ednaOpcija = document.querySelector('.invalidSelect');
const invalidSelect = document.querySelector('.selected');

const validnaOpcija = document.querySelector('.validSelect');

let valid = false;

let opt = document.getElementsByName('category');

function validate() {
  for (let i = 0; i < opt.length; i++) {
    if (opt[i].checked) {
      valid = true;
      break;
    }
  }
  if (valid) {
    console.log('imas opcija');
  } else {
    ednaOpcija.classList.toggle('visible');
    invalidSelect.classList.toggle('invalid');
  }
}

for (let i = 0; i < opt.length; i++) {
  if (opt[i]) {
    opt[i].addEventListener('click', validOption);
  }
  function validOption() {
    validnaOpcija.classList.toggle('visible');
    invalidSelect.classList.toggle('valid');
    ednaOpcija.classList.remove('visible');
  }
}

// navbar animation code
const hamburger = document.querySelector('.hamburger');
const hamClose = document.querySelector('.hamclose');
const navLinks = document.querySelector('.nav-links');
const links = document.querySelectorAll('.nav-links li');

hamburger.addEventListener('click', () => {
  navLinks.classList.toggle('open');
});

hamClose.addEventListener('click', () => {
  navLinks.classList.remove('open');
});

// bootstrap JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict';
  window.addEventListener(
    'load',
    function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            }
            form.classList.add('was-validated');
          },
          false
        );
      });
    },
    false
  );
})();
